﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwerveMovement : MonoBehaviour
{
    private SwerveInputSystem _swerveInputSystem;
    [SerializeField] private float swerveSpeed = 3f;
    [SerializeField] private float forwardSpeedFixedValue = 10f;
    public Rigidbody _rigidbody;
    private bool movementIsOn = true;

    public void Awake()
    {
        _swerveInputSystem = GetComponent<SwerveInputSystem>();
    }

    private void FixedUpdate()
    {
        if (movementIsOn && GameManager.LevelHasCompleted)
            Invoke("StopMovement", 1f);
        if (movementIsOn)
        {
            float forwardSpeed = _swerveInputSystem.isRunning ? forwardSpeedFixedValue : 0; 
            float swerveAmount = Time.fixedDeltaTime * swerveSpeed * _swerveInputSystem.MoveFactor;
            _rigidbody.AddForce(swerveAmount, 0, 0, ForceMode.VelocityChange);
            transform.LookAt(new Vector3(transform.position.x + _rigidbody.velocity.x, 0, transform.position.z + 3));
            _rigidbody.velocity = new Vector3(_rigidbody.velocity.x, _rigidbody.velocity.y, forwardSpeed);

            if (_rigidbody.position.y < -22 && GameManager.LevelHasCompleted == false) // if the player falls off the platform
                FindObjectOfType<PlayerCollision>().GoToStartPosition();
        }
        else
        {
            _rigidbody.velocity = new Vector3(0f, 0f, 0f);
        }
    }

    private void StopMovement()
    {
        movementIsOn = false;
    }
}
