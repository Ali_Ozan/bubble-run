﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameModeSetter : MonoBehaviour
{
    void Start()
    {
        if (SceneManager.GetActiveScene().buildIndex == 2)
            GameManager.gameModeIsRace = true;
        else
            GameManager.gameModeIsRace = false;
    }
}
