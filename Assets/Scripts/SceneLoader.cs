﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void LoadMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void LoadSoloRun()
    {
        GameManager.LevelHasCompleted = false;
        GameManager.LevelHasWon = false;
        SceneManager.LoadScene(1);
    }

    public void LoadRaceMode()
    {
        GameManager.LevelHasCompleted = false;
        GameManager.LevelHasWon = false;
        SceneManager.LoadScene(2);
    }
}
