﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalObstacleMovement : MonoBehaviour
{
    private Vector3 startPosition;
    private float randomStarterValue; //I don't want all the donuts to start at exactly the same position and move in sync. I want to see some kind of randomness.
    private float cycleHastener = 4f; //To adjust cycle times.
    private float travelDistanceSetter; //each horizontal obstacle will cover a different range
    void Start()
    {
        startPosition = transform.position;
        randomStarterValue = Random.value * 1000;
        travelDistanceSetter = Random.Range(70f, 100f);
    }

    void FixedUpdate()
    {
        transform.position = startPosition + new Vector3(Mathf.Sin(Time.time * cycleHastener + randomStarterValue) * Time.deltaTime * travelDistanceSetter, 0f, 0f);
    }
}
