﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateFireworks : MonoBehaviour
{
    public GameObject Fireworks;

    private void Update()
    {
        if (GameManager.LevelHasWon)
            Fireworks.SetActive(true);
    }
}
