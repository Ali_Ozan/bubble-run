﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpponentMovement : MonoBehaviour
{
    [SerializeField] private float forwardSpeedFixedValue = 7f;
    [SerializeField] private float swerveSpeed = 2f;
    public Rigidbody _rigidbody;
    private int numberOfRays = 23;
    private float angle = 110f;
    private float rayRange = 5.5f;
    private float distanceFactor = 1f;
    Ray ray;
    RaycastHit hitInfo;
    private Vector3 startPosition;
    private Quaternion startRotation;
    private Vector3 startEuler;
    private Vector3 startForward;

    private void Start()
    {
        startPosition = transform.position;
        startRotation = transform.rotation;
        startEuler = transform.eulerAngles;
        startForward = transform.forward;
    }

    void FixedUpdate()
    {
        float sidewaysPushFactor = 0f;

        for (int i = 0; i < numberOfRays; i++)
        {
            Quaternion rotation = transform.rotation;
            Quaternion rotationAngleAxis = Quaternion.AngleAxis((i / ((float)numberOfRays - 1)) * angle * 2 - angle, transform.up); //start from -110 to move to +110 //there will be 23 rays, but 22 angles between those rays, hence didn't use <= in for loop and used -1 when dividing
            Vector3 direction = rotation * rotationAngleAxis * Vector3.forward;

            ray = new Ray(transform.position + new Vector3(0, 2.4f, 0.51f), direction);

            if (Physics.Raycast(ray, out hitInfo, rayRange))
            {
                if (hitInfo.collider.tag == "Obstacle")
                {
                    sidewaysPushFactor -= (1.0f / (float)numberOfRays) * direction.x;
                    //distanceFactor += rayRange + 1 / (hitInfo.collider.transform.position - transform.position).magnitude ; //closer the object, stronger the pushing force will be
                }
            }
        }
        float swerveAmount = Time.fixedDeltaTime * swerveSpeed * sidewaysPushFactor * distanceFactor * 150f;
        distanceFactor = 1f;
        sidewaysPushFactor = 0f;
        _rigidbody.AddForce(swerveAmount, 0, 0, ForceMode.VelocityChange);
        _rigidbody.velocity = new Vector3(_rigidbody.velocity.x, _rigidbody.velocity.y, forwardSpeedFixedValue);
        transform.LookAt(new Vector3(transform.position.x + _rigidbody.velocity.x, 0, transform.position.z + 3));  //something like this might be used to make opponents look towards the direction they are running 
        transform.eulerAngles = startEuler;

        if (_rigidbody.position.y < -22f) // if the player falls off the platform
            GoToStartPosition();
    }

    private void OnDrawGizmos()
    {
        for (int i = 0; i < numberOfRays; i++)
        {
            Quaternion rotation = transform.rotation;
            Quaternion rotationAngleAxis = Quaternion.AngleAxis((i / ((float)numberOfRays - 1)) * angle * 2 - angle, transform.up); //start from -110 to move to +110 //there will be 23 rays, but 22 angles between those rays, hence didn't use <= in for loop and used -1 when dividing
            Vector3 direction = rotation * rotationAngleAxis * Vector3.forward;
            Gizmos.DrawRay(transform.position + new Vector3(0, 2.4f, 0.51f), direction);
        }
    }


    public void GoToStartPosition()
    {
        transform.position = startPosition;
        transform.rotation = startRotation;
        transform.eulerAngles = startEuler;
        transform.forward = startForward;
    }
}

