﻿using UnityEngine;

public class PlayerCollision : MonoBehaviour
{
    public SwerveMovement movement; // A reference to our player movement script
    private Vector3 startPosition;
    private Quaternion startRotation;
    private Vector3 startEuler;
    private Vector3 startForward;

    private void Start()
    {
        startPosition = transform.position;
        startRotation = transform.rotation;
        startEuler = transform.eulerAngles;
        startForward = transform.forward;
    }

    void OnCollisionEnter(Collision CollisionInfo)
    {
        if (CollisionInfo.collider.tag == "Obstacle") //Checking if the object we collide has a tag called "Obstacle"
        {
            movement.enabled = false;
            Invoke("GoToStartPosition", 1f);
        }
    }

    public void GoToStartPosition()
    {
        transform.position = startPosition;
        transform.rotation = startRotation;
        transform.eulerAngles = startEuler;
        transform.forward = startForward;
        movement.enabled = true;
    }
}