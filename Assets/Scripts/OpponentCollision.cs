﻿using UnityEngine;

public class OpponentCollision : MonoBehaviour
{
    public OpponentMovement movement; // A reference to our opponent movement script
    private Vector3 startPosition;
    private Quaternion startRotation;
    private Vector3 startEuler;
    private Vector3 startForward;

    private void Start()
    {
        startPosition = transform.position;
        startRotation = transform.rotation;
        startEuler = transform.eulerAngles;
        startForward = transform.forward;
    }

    void OnCollisionEnter(Collision CollisionInfo)
    {
        if (CollisionInfo.collider.tag == "Obstacle") //Checking if the object we collide has a tag called "Obstacle"
        {
            DisableMovement();
            Invoke("GoToStartPosition", 1f);
        }
    }

    void OnTriggerEnter()
    {
        Invoke("DisableMovement", 1f);
    }

    public void GoToStartPosition()
    {
        transform.position = startPosition;
        transform.rotation = startRotation;
        transform.eulerAngles = startEuler;
        transform.forward = startForward;
        movement.enabled = true;
    }

    private void DisableMovement()
    {
        movement.enabled = false;
    }
}
