﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickRotatorMovement : MonoBehaviour
{
    [SerializeField] private float rotationMultiplier = 60f; 
   
    void FixedUpdate()
    {
        transform.Rotate(0f, rotationMultiplier * Time.deltaTime, 0f);
    }
}
