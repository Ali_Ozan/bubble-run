﻿using UnityEngine;

public class BringVictoryText : MonoBehaviour
{
    public GameObject victoryText;
    
    void Update()
    {
        if (GameManager.LevelHasWon)
        {
            victoryText.SetActive(true);
        }
    }
}
