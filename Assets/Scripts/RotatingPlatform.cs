﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingPlatform : MonoBehaviour
{
    Rigidbody _rigidBody;
    [SerializeField] private float rollingSpeed = 1.2f;

    private void Start()
    {
        _rigidBody = GetComponent<Rigidbody>();
    }
    void FixedUpdate()
    {
        _rigidBody.angularVelocity = new Vector3(0f, 0f, rollingSpeed);
    }
}
