﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwerveInputSystem : MonoBehaviour
{
    private float _lastFrameFingerPosition_X;
    private float _moveFactor; //how much we are moving compared to the last frame
    public float MoveFactor => _moveFactor; // going to reach this from another script
    public bool isRunning = false;
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _lastFrameFingerPosition_X = Input.mousePosition.x;
            isRunning = true;
        }
        else if (Input.GetMouseButton(0))
        {
            _moveFactor = Input.mousePosition.x - _lastFrameFingerPosition_X;
            _lastFrameFingerPosition_X = Input.mousePosition.x;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            _moveFactor = 0f; //if the player are not touching the screen there should not be sideways movement
            isRunning = false;
        }
    }
}
