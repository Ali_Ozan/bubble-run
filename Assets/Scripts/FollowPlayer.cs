﻿using UnityEngine;
using Es.InkPainter.Sample;

public class FollowPlayer : MonoBehaviour
{
    public Transform player;
    public Vector3 offset;
    public MousePainter _mousePainter;
    public GameObject wallToPaint;
    Vector3 targetPosition;
    Quaternion wantedRotation;
    float cameraTransitionSpeed = 1f;

    private void Start()
    {
        targetPosition = new Vector3(wallToPaint.transform.position.x, wallToPaint.transform.position.y + 4f, wallToPaint.transform.position.z - 20f);
        wantedRotation = Quaternion.LookRotation(new Vector3(0f, -0.2f, 1f), new Vector3(0, 1f, 0));
    }

    void Update()
    {
        if (GameManager.LevelHasCompleted)
        {
            transform.position = Vector3.Lerp(transform.position, targetPosition, Time.deltaTime * cameraTransitionSpeed);
            transform.rotation = Quaternion.Lerp(transform.rotation, wantedRotation, Time.deltaTime * 0.7f);
            Invoke("EnablePainter", 2f);//just to avoid painting the ball immediately while actually touching to run
        }
        else
            transform.position = player.position + offset;
    }


    private void EnablePainter()
    {
        _mousePainter.enabled = true;
    }
}
