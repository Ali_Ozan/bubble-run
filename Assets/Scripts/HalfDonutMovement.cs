﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HalfDonutMovement : MonoBehaviour
{
    private Vector3 startPosition;
    private float randomStarterValue; //I don't want all the donuts to start at exactly the same position and move in sync. I want to see some kind of randomness.
    private float cycleHastener = 4f; //To adjust cycle times.
    [SerializeField] private float travelDistanceSetter = 75f;
    void Start()
    {
        startPosition = transform.position;
        randomStarterValue = Random.value * 1000;
    }

    void FixedUpdate()
    {
        transform.position = startPosition + new Vector3(Mathf.Sin(Time.time * cycleHastener + randomStarterValue) * Time.deltaTime * travelDistanceSetter, 0f, 0f);
    }
}
