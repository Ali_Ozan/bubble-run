﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class GameManager : MonoBehaviour
{
    public bool gameHasEnded = false;
    static public bool LevelHasCompleted = false;
    static public bool LevelHasWon = false;
    static public bool gameModeIsRace = false;
    public float restartDelay = 1f;

    int playerRank = 1;
    public Text playerRankText;
    public GameObject Opponents;
    public GameObject Player;
    private List<float> OpponentZs = new List<float>();


    private void Update()
    {
        if (!LevelHasCompleted && gameModeIsRace)
        {
            DeterminePlayerRank();
        }
        if (LevelHasCompleted && playerRank == 1)
        {
            LevelHasWon = true;
        }
    }

    private void DeterminePlayerRank()
    {
        OpponentZs.Clear(); 
        playerRank = 1;
        for (int i = 0; i<Opponents.transform.childCount; i++)
          {
            OpponentZs.Add(Opponents.transform.GetChild(i).position.z);
          }
        for (int i = 0; i < OpponentZs.Count; i++)
        {
            if (OpponentZs[i] > Player.transform.position.z)
            {
                playerRank++;
            }
        }

        if (playerRank == 1)
            playerRankText.text = playerRank.ToString() + "st";
        else if (playerRank == 2)
            playerRankText.text = playerRank.ToString() + "nd";
        else if (playerRank == 3)
            playerRankText.text = playerRank.ToString() + "rd";
        else
            playerRankText.text = playerRank.ToString() + "th";
    }

    public void CompleteLevel()
    {
        LevelHasCompleted = true;
    }

    public void RestartGame()
    {
        if (gameHasEnded == false)
        {
            gameHasEnded = true;
            Invoke("Restart", restartDelay);
        }
    }

    public void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}