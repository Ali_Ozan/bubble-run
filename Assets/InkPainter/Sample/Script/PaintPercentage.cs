﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PaintPercentage : MonoBehaviour
{
    Material wallMaterial;
    public Color targetColor;
    private Texture2D wallTexture;
    private Color[] texturePixels;
    

    void Awake()
    {
        wallMaterial = GetComponent<Renderer>().material;
        wallTexture = wallMaterial.mainTexture as Texture2D;
        texturePixels = wallTexture.GetPixels();
    }

    void Update()
    {
        if (GameManager.LevelHasCompleted)
        {
            /* This does not work somehow. I could not figure out why yet, but the approach should be something similar to this I guess
            int coloredPixelCount = 0;
            for (int i = 0; i < texturePixels.Length; i++)
            {
                if (texturePixels[i].r == targetColor.r && texturePixels[i].g == targetColor.g && texturePixels[i].b == targetColor.b)
                {
                    coloredPixelCount++;
                }
            }

            float percentageComnpleted = coloredPixelCount / texturePixels.Length;
            */
        }
    }
}
